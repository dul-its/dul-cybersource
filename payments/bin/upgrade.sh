#!/usr/bin/env bash

OPTS=`getopt -o i:t:dDu -l imgtag:,tls-secret:,dry-run,debug,uninstall -n 'upgrade.sh' -- "$@"`
if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1; fi

eval set -- "$OPTS"

IMAGE_TAG="latest"
TLS_SECRETNAME=$(oc project -q)-tls
DRY_RUN=""
DEBUG=0
UNINSTALL=0

while true; do
  case "$1" in
    -i | --imgtag ) IMAGE_TAG="$2"; shift 2 ;;
    -t | --tls-secret ) TLS_SECRETNAME="$2"; shift 2 ;;
    -d | --dry-run ) DRY_RUN="--dry-run "; shift ;;
    -D | --debug ) DEBUG=1; shift ;;
    -u | --uninstall ) UNINSTALL=1; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if [[ $UNINSTALL -eq 1 ]]; then
  echo "Uninstalling ${1} first..."
  helm uninstall ${1}
fi

INGRESS_TLS_SECRET="--set ingress.tls[0].secretName=${TLS_SECRETNAME}"

if [[ $DEBUG -eq 1 ]]; then
  echo "Release name = ${1}"
  echo "Chart location = ${2:-.}"
  echo $IMAGE_TAG
  echo $DRY_RUN
  echo $TLS_SECRETNAME
  echo $INGRESS_TLS_SECRET
  echo $SET_INGRESS_ENABLE
  exit 0
fi

helm upgrade --install \
  --values values-${1}.yaml \
  --set image.tag=${IMAGE_TAG} \
  ${INGRESS_TLS_SECRET} \
  ${DRY_RUN}$1 ${2:-.}

# helm upgrade --install --values values-pre.yaml --set ingress.tls[0].secretName=$(oc project -q)-tls --set serviceAccount.create=false pre
