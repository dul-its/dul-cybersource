# Helm-based Deployment
[[_TOC_]]

## Prerequisites

### Use OpenShift CLI
**BEFORE PROCEEDING FURTHER**, make sure you:
* have installed `oc`, `helm`, *AND*
* have connected to the OKD instance.
  
***Need a refresher?***  
[Installing (and Using) OpenShift CLI and Helm](https://gitlab.oit.duke.edu/devops/antsy/-/wikis/Installing-OpenShift-and-Helm)
  
Now, you're ready to proceed.

## TL;DR

### HOSTS/DOMAINS and OKD
DevOps can create CNAME records in aka.oit.duke.edu. For this application, we'll create:  
| Hostname (CNAME) | Points to... | Network View (AKA) |
| ---              | ---          | ---                |
| payments-pre.library.duke.edu | ingress.dev.okd4.fitz.cloud.duke.edu | Internal |
| payments.library.duke.edu | ingress.prod.okd4.fitz.cloud.duke.edu | Internal |
| payments.library.duke.edu | external.ingress.prod.okd4.fitz.cloud.duke.edu | External |
  
**Note about creating CNAMES for non-Library hosts**  
If you're looking to create a new SSL certificate for use with an OIT OKD4 cluster, then do this first:
* Temporarily point your CNAME to any Library-based host (e.g. `automation.lib.duke.edu`)
* Initiate the SSL request at https://locksmith.oit.duke.edu (DevOps have permission here)
* Point your CNAME to the appropriate host (above).
  
### Creating Secrets
While the templates (under `/templates`) take care of most aspects of deploying an application instance, 
you'll need to create secrets to store privileged data. The typical kinds of secrets are:  
* TLS (certificate/private key pair)
* Database
* Other application related secrets

For "Payments", we'll need to create both a TLS secret and a generic secret to store Cybersource-related data.

#### TLS Secret
  
```sh
# Do your oc login
$ oc login --token=<get-your-token-from-okd4-console> --server=https://https://api.prod.okd4.fitz.cloud.duke.edu:6443

# make sure you're in the right namespace
oc project dul-payments-prod  # or 'dul-payments-pre'
oc create secret tls $(oc project -q)-tls \
  --cert=/path/to/cert \
  --key=/path/to/key \
```
**Note about certificates**  
  
The author typically uses a "bundle (root last)" certificate as opposed to 
a simple certicate (w/o the intermediate CA stuff).
  
#### Cybersource-specific secrets (Generic)
```sh
# Do your oc login
$ oc login --token=<get-your-token-from-okd4-console> --server=https://https://api.prod.okd4.fitz.cloud.duke.edu:6443

# make sure you're in the right namespace
oc project dul-payments-prod  # or 'dul-payments-pre'

$ oc create secret generic <secret-name> \
  --from-literal=cybersource_access_key=<value-here> \
  --from-literal=cybersource_secret_key=<value-here> \
  --from-literal=cybersource_profile_id=<value-here> \
  ...
```

### Build Image and Push To K8S Imagestream
The secrets are in place and you're ready to deploy your image!  
  
But wait... you don't have an image to deploy yet. Let's fix that.
#### Docker Login
```bash
# if you haven't already, do the "oc login" stuff (copy token from console)
$ docker login -u openshift -p $(oc whoami -t) ${OKD_REGISTRY_HOST}
```
...where `${OKD_REGISTRY_HOST}` is one of:

| OKD4 Cluster | Registry Hostname |
| ---          | ---          |
| DEV | registry.apps.dev.okd4.fitz.cloud.duke.edu |
| PROD | registry.apps.prod.okd4.fitz.cloud.duke.edu |
  
***Hint***  
One could create local environment variables, `OKD_REGISTRY_DEV` and `OKD_REGISTRY_PROD` - referring 
to these as an exercise in "shorthand".

#### Build/Tag/Push Your Image

***
Note: If you are building your image using an ARM-based (M1, M2, etc) mac, you need to [change the default build platform](https://stackoverflow.com/questions/65612411/forcing-docker-to-use-linux-amd64-platform-by-default-on-macos/69636473#69636473) to force compatiillbity with linux. Run this command to set the default platform temporarily before building (and make sure to reload the console):
```
export DOCKER_DEFAULT_PLATFORM=linux/amd64
```

when finished building your image, you can run the following to restore back to normal:
```
export DOCKER_DEFAULT_PLATFORM=linux/arm64
```
***

```bash
$ cd /path/to/dul-cybersourse
$ docker build -t ${OKD_REGISTRY_HOST}/$(oc project -q)/payments:latest -f .docker/Dockerfile .
$ docker push ${OKD_REGISTRY_HOST}/$(oc project -q)/payments:latest
```
### Verify The Imagestream
```bash
$ oc get imagestreams  # or 'oc get is'

# then...
$ oc describe imagestream/payments  # or 'oc describe is/payments'
```

### Install/Re-install Payments Application
```bash
$ cd /path/to/dul-cybersource/payments

$ oc project dul-payments-prod # production
# oc project dul-payments-pre for staging

# production
$ helm upgrade --install prod \
    --values values-prod.yaml \
    --set ingress.tls[0].secretName=$(oc project -q)-tls \
    --set image.tag=[LATEST_TAGGED_RELEASE_FROM_GITLAB] \
    .
# locate the latest tagged release in this project's repo
# https://gitlab.oit.duke.edu/dul-its/dul-cybersource/-/tags
#
# or
# git tag -l --sort=-v:refname

# ----

# or "pre"
$ helm upgrade --install pre \
    --values values-pre.yaml \
    --set ingress.tls[0].secretName=$(oc project -q)-tls
    --set image.tag=latest \
    .
```

#### GitLab CI/CD Note
In the project's `.gitlab-ci.yml` file, we need to use `--kube-apiserver` when calling 
`helm upgrade --install`.  

 
## "From-Scratch" Setup
FOR DOCUMENTATION PURPOSES  
  
This will likely not be necessary once the application is 
running.  
  
```
cd /path/to/dul-cybersource/payments
```  
`payments` is the directory that contains our Helm chart.  
  

### Values Overrides
When installing our helm chart, the values in `values.yaml` will be used to set up the instance -- along with the templates.  
  
For specific settings for a particular instance (dev, pre, prod, etc), you can create override values. For instance: 
```yaml
ingress:
  enabled: true
  className: ""
  annotations: {}
  hosts:
    - host: payments.library.duke.edu
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls:
    # The 'secretName' variable is defined by left intentionally blank.
    # -
    # Set this value in the CLI call to "helm install" 
    # or "helm upgrade --install"
    # using:
    # --values values-(pre|prod).yaml \
    # --set ingress.tls[0].secretName=$(oc project -q)-tls
    - secretName: 
      hosts:
        - payments.library.duke.edu

```
  

