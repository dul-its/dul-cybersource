<?php
?>

<html lang="en" dir="ltr">

<head>

  <title>Interlibrary Request Payments - Generate Invoice | Duke University Libraries</title>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;1,300;1,400;1,600&display=swap" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/all.min.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" crossorigin="anonymous">

  <link type="text/css" rel="stylesheet" href="/css/styles.css" media="all" />

</head>

<body>

<div class="pt-4 pb-4 container bg-white" role="main" id="content">

  <div class="col-lg-12">

    <h1>Generate DDS Invoice</h1>

    <form action="javascript:void(0);">

      <div class="form-group col-md-6 pt-3">

        <label for="payment-type">Payment Type:</label>

          <div class="form-check">
            <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-ill" value="InterlibraryLoan">
            <label for="payment-type-ill">Interlibrary Loan</label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-dd" value="DocumentDelivery">
            <label for="payment-type-dd">Document Delivery Article</label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-lost" value="LostItem">
            <label for="payment-type-lost">Lost Item</label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-damaged" value="DamagedItem">
            <label for="payment-type-damaged">Damaged Item</label>
          </div>

      </div>

      <div class="form-group col-md-6">
        <label for="reference_number">Invoice Number:</label>
        <input class="form-control" type="text" name="reference_number" id="reference_number">
      </div>

      <div class="form-group col-md-6">
        <label for="ill_number">ILL Number:</label>
        <input class="form-control" type="text" name="ill_number" id="ill_number">
      </div>

      <div class="form-group col-md-6">
        <label for="amount">Amount:</label>
        <input class="form-control" type="text" name="amount" id="amount">
      </div>

      <div class="form-group col-md-6">
        <input class="btn btn-primary" type="submit" id="submit" value="Generate URL">
      </div>


      <div class="form-group pt-5">
        <label for="output">The URL:</label>
        <textarea class="form-control" id="output" name="output" readonly> </textarea>
      </div>

    </form>

  </div>

</div>


<script type="text/javascript">

$( "#submit" ).click(function() {

  var reference_number = jQuery("#reference_number").val();

  var ill_number = jQuery("#ill_number").val();

  var amount = jQuery("#amount").val();

  var paytype = jQuery("input[name=paytype]:checked").val();


  //$('#output').text('http://payments.library.duke.edu/rubenstein/index.php?reference_number=' + reference_number + '&amount=' + amount + '&librarycontact=' + librarycontact);

  $('#output').text('https://payments.library.duke.edu/dds/index.php?');

  if (reference_number != '') {
    $('#output').append('reference_number=' + reference_number);
  }

  if (amount != '') {

    if (reference_number != '') {
      $('#output').append('&');
    }

    $('#output').append('amount=' + amount);
  }

  if (paytype != '') {

    if (amount != '' || reference_number != '') {
      $('#output').append('&');
    }

    $('#output').append('payment-type=' + paytype);
  }

  if (ill_number != '') {

    if (amount != '' || reference_number != '' || paytype != '') {
      $('#output').append('&');
    }

    $('#output').append('ill_number=' + ill_number);
  }

});

</script>

</body>

</html>
