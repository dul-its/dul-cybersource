<html lang="en" dir="ltr">

<head>

  <title>DPC Payments - Generate Invoice | Duke University Libraries</title>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;1,300;1,400;1,600&display=swap" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/all.min.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" crossorigin="anonymous">

  <link type="text/css" rel="stylesheet" href="/css/styles.css" media="all" />

</head>

<body>

<!-- CONTENT -->
<div class="pt-4 pb-4 container bg-white" role="main" id="content">

  <div class="col-lg-12">

    <h1>Generate DPC Invoice</h1>

    <form action="javascript:void(0);">

      <div class="form-group col-md-3">
        <label for="staff_initials">Staff Initials:</label>
        <input class="form-control" type="text" name="staff_initials" id="staff_initials" size="3">
      </div>

      <div class="form-group col-md-6 pt-3">

        <label for="department">Department:</label>

          <div class="form-check">
            <input class="form-check-input" type="radio" required="required" name="department" id="department-1" value="RS">
            <label for="department-1">Research Services</label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" required="required" name="department" id="department-2" value="DC">
            <label for="department-2">DPC</label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" required="required" name="department" id="department-3" value="HC">
            <label for="department-3">Hartman Center</label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" required="required" name="department" id="department-4" value="UA">
            <label for="department-4">University Archives</label>
          </div>

      </div>

      <div class="form-group col-md-6">
        <label for="daily">Daily Invoice Number:</label>
        <input class="form-control" type="text" name="daily" id="daily">
      </div>

      <div class="form-group col-md-6">
        <label for="amount">Amount:</label>
        <input class="form-control" type="text" name="amount" id="amount">
      </div>

      <div class="form-group col-md-6">
        <label for="librarycontact">Library Contact:</label>
        <input class="form-control" type="text" name="librarycontact" id="librarycontact">
      </div>

      <div class="form-group col-md-6">
        <input class="btn btn-primary" type="submit" id="submit" value="Generate URL">
      </div>


      <div class="form-group pt-5">
        <label for="output">The URL:</label>
        <textarea class="form-control" id="output" name="output" readonly> </textarea>
      </div>

    </form>

  </div>

</div>

<script type="text/javascript">

$( "#submit" ).click(function() {

  // get date
  var d = new Date();
  var month = d.getMonth()+1;
    if (month < 10) {
        month = '0' + month;
    }
  var day = d.getDate();
    if (day < 10) {
        day = '0' + day;
    }
  var year = d.getFullYear();

  var staff_initials = jQuery("#staff_initials").val();
  var department = jQuery("input[type='radio'][name='department']:checked").val();
  var daily = jQuery("#daily").val();

  // combine everything
  var reference_number = year + '-' + month + '-' + day + '-' + staff_initials + '-' + daily + '-' + department;

  var amount = jQuery("#amount").val();

  var librarycontact = jQuery("#librarycontact").val();

  $('#output').text('https://payments.library.duke.edu/dpc/index.php?');

  if (reference_number != '') {
    $('#output').append('reference_number=' + reference_number);
  }

  if (amount != '') {

    if (reference_number != '') {
      $('#output').append('&');
    }

    $('#output').append('amount=' + amount);
  }

  if (librarycontact != '') {

    if (amount != '' || reference_number != '') {
      $('#output').append('&');
    }

    $('#output').append('librarycontact=' + librarycontact);
  }

});

</script>

</body>

</html>
