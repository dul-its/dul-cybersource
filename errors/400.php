<html lang="en" dir="ltr">
<head>

	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">

    <title>Payments - Error | Duke University Libraries</title>

    <?php include '../includes/header_scripts.php' ?>

</head>
<body>

  <div id="skip-link">
    <a href="#content" class="element-invisible element-focusable">Skip to main content</a>
  </div>

	<!-- HEADER -->
	<div class="header" role="banner">
		<!-- This is the basic masthead filler that is replaced via javascript -->
		<div id="dul-masthead-filler" style="width: 100%; text-align: center;">
		  <div class="dul-masthead-wrapper" style="height: 105px; width: 100%; max-width: 1220px; background-color:#053482; margin: 0 auto; margin-bottom: -2px;">
			  <a href="//library.duke.edu/"><img src="//library.duke.edu/masthead/img/logo.png" alt="Duke University Libraries" id="dul-masthead-logo" style="float: left; margin: 20px;"></a>
		  </div>
		</div>
	</div>

	<!-- BREADCRUMBS -->
	<div id="breadcrumbs" class="container">
		<?php include '../includes/breadcrumb.php' ?>
	</div>


	<!-- CONTENT -->
	<div class="pt-4 pb-4 container bg-white" role="main" id="content">

		<div class="col-lg-12">

			<?php include '../includes/message.php' ?>

			<h1>Payments Error</h1>

			<div class="container">
				<div class="row pt-3 pb-2">
					<div class="col-md-8">
						<p>This page cannot be found or you do not have access to it.</p>
					</div>
					<div class="col-md-4 sidebar" role="complementary">
						<p><a href="https://library.duke.edu/services/forms/webmaster">Contact us</a> if you need more assistance!</p>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- FOOTER -->
	<?php include "../includes/footer.php" ?>

</div>

</body>
</html>
