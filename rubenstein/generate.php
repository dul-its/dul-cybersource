<?php
?>

<html lang="en" dir="ltr">

<head>

<script type="text/javascript" src="/js/jquery.min.js"></script>

<script type="text/javascript" src="/js/bootstrap.min.js"></script>

<style type="text/css">

.field {
  display: block;
  margin: 0 0 1em 0;
  clear: both;
}

.field label {
    display: block;
    margin: 0 .25em 0 0;
}

.field input {
    display: block;
}

.radio {
    clear: both;
    margin: 0 0 1em 0;
    display: block;
    overflow: hidden;
}

.radio input {
    float: left;
    clear: left;
    margin: 0 .5em 0 .5em;
}

.radio label {
    float: left;
}


#submit {
    margin: 0 0 5em 0;
    background: #ccc;
}

#output {
    width: 50em;
    height: 5em;
    display: block;
}

</style>

</head>

<body>

<h1>Generate RL Invoice</h1>

<!--
<div class="field">
<label for="reference_number">Invoice Number:</label>
<input type="text" name="reference_number" id="reference_number">
</div>
-->

<div class="field">
<label for="staff_initials">Staff Initials:</label>
<input type="text" name="staff_initials" id="staff_initials" size="3">
</div>

<div class="field radio">
<div class="header" role="banner">Department:</div>

    <input type="radio" id="department-1" name="departments" value="RS"> <label for="department-1">Research Services</label>

    <input type="radio" id="department-2" name="departments" value="DC"> <label for="department-2">DPC</label>

    <input type="radio" id="department-3" name="departments" value="HC"> <label for="department-3">Hartman Center</label>

    <input type="radio" id="department-4" name="departments" value="UA"> <label for="department-4">University Archives</label>

</div>



<div class="field">
<label for="amount">Daily Invoice Number:</label>
<input type="text" name="daily" id="daily" size="1" value="1">
</div>


<div class="field">
<label for="amount">Amount:</label>
<input type="text" name="amount" id="amount" size="10">
</div>

<div class="field">
<label for="librarycontact">Library Contact:</label>
<input type="text" name="librarycontact" id="librarycontact" size="40">
</div>


<input type="submit" id="submit" value="Generate URL">

<div class="field">
<label for="output">The URL:</label>
<textarea id="output" name="output"> </textarea>
</div>


<script type="text/javascript">

$( "#submit" ).click(function() {

  // get date
  var d = new Date();
  var month = d.getMonth()+1;
    if (month < 10) {
        month = '0' + month;
    }
  var day = d.getDate();
    if (day < 10) {
        day = '0' + day;
    }
  var year = d.getFullYear();

  var staff_initials = jQuery("#staff_initials").val();
  var department = jQuery("input[type='radio'][name='departments']:checked").val();
  var daily = jQuery("#daily").val();

  // combine everything
  var reference_number = year + '-' + month + '-' + day + '-' + staff_initials + '-' + daily + '-' + department;

  var amount = jQuery("#amount").val();

  var librarycontact = jQuery("#librarycontact").val();

  //$('#output').text('http://payments.library.duke.edu/rubenstein/index.php?reference_number=' + reference_number + '&amount=' + amount + '&librarycontact=' + librarycontact);

  $('#output').text('https://payments.library.duke.edu/rubenstein/index.php?');

  if (reference_number != '') {
    $('#output').append('reference_number=' + reference_number);
  }

  if (amount != '') {

    if (reference_number != '') {
      $('#output').append('&');
    }

    $('#output').append('amount=' + amount);
  }

  if (librarycontact != '') {

    if (amount != '' || reference_number != '') {
      $('#output').append('&');
    }

    $('#output').append('librarycontact=' + librarycontact);
  }

});

</script>

</body>

</html>
