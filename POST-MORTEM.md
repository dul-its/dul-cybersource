# Post Mortem / Lessons Learned

## PHP/Apache Image & OKD
OIT's OKD setup does not support/allow containers to run servers on "privileged ports" (1-1023).  
  
The `php:8.3-apache` image is built to run its Apache web server on ports 80 and 443. We needed to somehow 
configure the image to run, only, on ports 8080 and 8443.

### Solution
We did the following things:

#### Run Local Image
Run the `php:8.3-apache` locally in interactive mode to find the location of the config file(s) 
we intended to change.

#### Edit Our Dockerfile
Knowing that `/etc/apache2/ports.conf` was the file in question, we added the following two 
lines to our Dockerfile (`.docker/Dockerfile`):
  
```Dockerfile
RUN sed -i -r 's/Listen 80/Listen 8080/' /etc/apache2/ports.conf
RUN sed -i -r 's/Listen 43/Listen 8443/'/etc/apache2/ports.conf
```
  
With these two lines, we're able to prevent the container's web server from binding 
to the "privileged ports".
  

