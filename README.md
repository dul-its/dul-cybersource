# Project "cybersource"

This project runs as:
- **payments.library.duke.edu** (production)
- **payments-pre.library.duke.edu** (testing)
  
## Deployment Notes
Continuous Integration (CI) has been enabled and configured for this project.  That, along with a Dockerfile 
will ensure seamless automated deployments for the project.  

- before merging, make sure to bump the veersion number under /payments/Chart.yml
- merge your development branch into develop
- merge latest develop into master
- after merging, wait for CI to finish, then test on payments-pre.library.duke.edu
- once verified all is well, you can create a release (under Deployments > Releases) and use the version number above to create a new tag
- when release is saved, that triggers the deployment to production


## Local Testing Notes
- copy 'env-example' to 'dev.env' and update secrets
- run 'docker-compose up --build'
- project will be available at localhost:80
