<?php include '../includes/security.php' ?>

<?php

// reassign values

$_REQUEST['merchant_defined_data2'] = $_REQUEST['payment_other'];

$_REQUEST['merchant_defined_data3'] = $_REQUEST['librarycontact'];

$_REQUEST['merchant_defined_data4'] = $_REQUEST['librarydepartment'];

$_REQUEST['merchant_defined_data5'] = $_REQUEST['additional-comments'];

?>

<html lang="en" dir="ltr">
<head>

	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">

    <title>Other Payments - Verify Information | Duke University Libraries</title>

    <?php include '../includes/header_scripts.php' ?>

</head>
<body>

  <div id="skip-link">
    <a href="#content" class="element-invisible element-focusable">Skip to main content</a>
  </div>

	<!-- HEADER -->
	<div class="header" role="banner">
		<!-- This is the basic masthead filler that is replaced via javascript -->
		<div id="dul-masthead-filler" style="width: 1220px; text-align: center;">
		  <div class="dul-masthead-wrapper" style="height: 105px; width: 100%; max-width: 1220px; background-color:#053482; margin: 0 auto; margin-bottom: -2px;">
			  <a href="//library.duke.edu/"><img src="//library.duke.edu/masthead/img/logo.png" alt="Duke University Libraries" id="dul-masthead-logo" style="float: left; margin: 20px;"></a>
		  </div>
		</div>
	</div>

	<!-- BREADCRUMBS -->
	<div id="breadcrumbs" class="container">
		<?php include '../includes/breadcrumb_other.php' ?>
	</div>


	<!-- CONTENT -->
	<div class="pt-4 pb-4 container bg-white" role="main" id="content">
		<div class="col-lg-12">

			<h1>Other Payments</h1>

			<div class="container">
				<div class="row pt-3 pb-2">
					<div class="col-md-1"></div>
					<div class="col-md-10">

					<?php


					if (!isset($_REQUEST)) {

						echo "<p>There was an error!<p>";


					} else {

						$errorMsg = "";


						if (empty($_REQUEST['payment_other'])) {
							$errorMsg .= "<p>Please go back and enter what you are paying for.</p>";
						}

						if ($_REQUEST['amount'] == ""){
							$errorMsg .= "Please go back and enter a Payment Amount.\r\n\n";
						}

						if ($_REQUEST['reference_number'] == ""){
							$errorMsg .= "Please go back and enter an Invoice Number.\r\n\n";
						}

						if ($_REQUEST['librarycontact'] == ""){
							$errorMsg .= "<p>Please go back and enter the Library Contact Name.</p>";
						}

						if ($_REQUEST['librarydepartment'] == ""){
							$errorMsg .= "<p>Please go back and enter the Library Department name.</p>";
						}

						if ($_REQUEST['bill_to_forename'] == ""){
							$errorMsg .= "Please go back and enter your First Name.\r\n\n";
						}

						if ($_REQUEST['bill_to_surname'] == ""){
							$errorMsg .= "Please go back and enter your Last Name.\r\n\n";
						}

						if ($_REQUEST['bill_to_email'] == ""){
							$errorMsg .= "Please go back and enter your Email Address.\r\n\n";
						}

						if ($_REQUEST['bill_to_email'] != ""){

							if ($_REQUEST['bill_to_email'] != $_REQUEST['bill_to_email_confirmation']){
								$errorMsg .= "<p>Please go back and verify your Email Address.</p>";
							}

							if(filter_var($_REQUEST['bill_to_email'], FILTER_VALIDATE_EMAIL)) {
								// valid address
							}
							else {
								// invalid address
								$errorMsg .= "Please go back and enter a valid Email Address.\r\n\n";
							}

						}

						if ($_REQUEST['bill_to_address_line1'] == ""){
							$errorMsg .= "Please go back and enter your Billing Address.\r\n\n";
						}

						if ($_REQUEST['bill_to_address_city'] == ""){
							$errorMsg .= "Please go back and enter your City.\r\n\n";
						}

						if ($_REQUEST['bill_to_address_country'] == "US") {

							if ($_REQUEST['bill_to_address_state'] == ""){
								$errorMsg .= "Please go back and choose your State.\r\n\n";
							}

						}

						if ($_REQUEST['bill_to_address_country'] == "CA") {

							if ($_REQUEST['bill_to_address_state_ca'] == ""){
								$errorMsg .= "Please go back and choose your Province.\r\n\n";
							}

						}

						if ($_REQUEST['bill_to_address_country'] == ""){
							$errorMsg .= "Please go back and choose your Country.\r\n\n";
						}

						if ($_REQUEST['bill_to_address_postal_code'] == ""){
							$errorMsg .= "Please go back and enter your Postal Code.\r\n\n";
						}



						if ($errorMsg != "") {
							echo '<div class="error">' . htmlspecialchars($errorMsg, ENT_QUOTES, 'UTF-8') . '</div>';
						}

						else {

					?>

									<form id="payment_confirmation" action="<?php echo getenv('CYBERSOURCE_CONFIRMATION_URL'); ?>" method="post"/>

									<?php

										if ($_REQUEST['bill_to_address_country'] == "CA") {

											$_REQUEST['bill_to_address_state'] = $_REQUEST['bill_to_address_state_ca'];

										}

										if ($_REQUEST['bill_to_address_country'] != "US") {

											if ($_REQUEST['bill_to_address_country'] != "CA") {

												$_REQUEST['bill_to_address_state'] = $_REQUEST['bill_to_address_state_int'];

											}

										}

										foreach($_REQUEST as $name => $value) {
											$params[$name] = $value;
										}

									?>

									<fieldset id="confirmation">

										<legend>Review Your Information</legend>

										<h4>Payment Information</h4>

										<p><span class="title">Paying For:</span> <?php echo htmlspecialchars($_REQUEST['merchant_defined_data2'], ENT_QUOTES, 'UTF-8'); ?></p>

										<p><span class="title">Payment Amount:</span> $<?php echo htmlspecialchars($_REQUEST['amount'], ENT_QUOTES, 'UTF-8'); ?></p>

										<p><span class="title">Invoice Number: </span> <?php echo htmlspecialchars($_REQUEST['reference_number'], ENT_QUOTES, 'UTF-8'); ?></p>

										<p><span class="title">Library Contact Name: </span> <?php echo htmlspecialchars($_REQUEST['librarycontact'], ENT_QUOTES, 'UTF-8'); ?></p>

										<p><span class="title">Library Department: </span> <?php echo htmlspecialchars($_REQUEST['librarydepartment'], ENT_QUOTES, 'UTF-8'); ?></p>


										<br />
										<h4>Your Information</h4>

										<p><?php echo htmlspecialchars($_REQUEST['bill_to_forename'], ENT_QUOTES, 'UTF-8') . '&nbsp;' . htmlspecialchars($_REQUEST['bill_to_surname'], ENT_QUOTES, 'UTF-8'); ?><br />

										<?php

										echo htmlspecialchars($_REQUEST['bill_to_email'], ENT_QUOTES, 'UTF-8') . '<br /><br />';

										if ($_REQUEST['bill_to_phone'] != "") {
											echo htmlspecialchars($_REQUEST['bill_to_phone'], ENT_QUOTES, 'UTF-8') . '<br />';
										}

										if ($_REQUEST['bill_to_company_name'] != "") {
											echo htmlspecialchars($_REQUEST['bill_to_company_name'], ENT_QUOTES, 'UTF-8') . '<br />';
										}

										echo htmlspecialchars($_REQUEST['bill_to_address_line1'], ENT_QUOTES, 'UTF-8') . '<br />';

										if ($_REQUEST['bill_to_address_line2'] != "") {
											echo htmlspecialchars($_REQUEST['bill_to_address_line2'], ENT_QUOTES, 'UTF-8') . '<br />';
										}

										echo htmlspecialchars($_REQUEST['bill_to_address_city'], ENT_QUOTES, 'UTF-8') . ', ' . htmlspecialchars($_REQUEST['bill_to_address_state'], ENT_QUOTES, 'UTF-8') . '&nbsp ' . htmlspecialchars($_REQUEST['bill_to_address_postal_code'], ENT_QUOTES, 'UTF-8') . '&nbsp; (' . htmlspecialchars($_REQUEST['bill_to_address_country'], ENT_QUOTES, 'UTF-8') . ')<br />';


										?>

										</p>

										<p><span class="title">Additional Comments: </span> <?php echo htmlspecialchars($_REQUEST['additional-comments'], ENT_QUOTES, 'UTF-8'); ?></p>


									<?php
										foreach($params as $name => $value) {
											echo "<input type=\"hidden\" id=\"" . htmlspecialchars($name, ENT_QUOTES, 'UTF-8') . "\" name=\"" . htmlspecialchars($name, ENT_QUOTES, 'UTF-8') . "\" value=\"" . htmlspecialchars($value, ENT_QUOTES, 'UTF-8') . "\"/>\n";
										}
										echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . htmlspecialchars(sign($params), ENT_QUOTES, 'UTF-8') . "\"/>\n";
									?>

									<br />

									<div class="submit-wrapper">
											<input type="submit" class="btn btn-primary" value="Continue to Payment Method &raquo;">
									</div>

									</form>



					<?php


						}

					}

					?>

				</div>
			</div>
		</div>
	</div>
</div>

	<!-- FOOTER -->
	<?php include "../includes/footer.php" ?>

</div>

</body>
</html>
