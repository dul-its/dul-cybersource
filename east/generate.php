<html lang="en" dir="ltr">

<head>

  <title>Lilly Library Payments - Generate Invoice | Duke University Libraries</title>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;1,300;1,400;1,600&display=swap" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/all.min.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" crossorigin="anonymous">

  <link type="text/css" rel="stylesheet" href="/css/styles.css" media="all" />

</head>

<body>

<!-- CONTENT -->
<div class="pt-4 pb-4 container bg-white" role="main" id="content">

  <div class="col-lg-12">

    <h1>Generate Lilly Library Invoice</h1>

    <form action="javascript:void(0);">

      <div class="col-md-10 pt-3">

        <label for="payment-type">Payment Type:</label>

        <div class="form-row">

          <div class="form-group col-md-6">

            <div class="form-check">
              <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-overdue-lilly" value="OverdueRecallLilly">
              <label for="payment-type-overdue-lilly">Lilly Overdue/Recall Fine</label>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-lost-lilly" value="LostItemLilly">
              <label for="payment-type-lost-lilly">Lilly Lost Item</label>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-damaged-lilly" value="DamagedItemLilly">
              <label for="payment-type-damaged-lilly">Lilly Damaged Item</label>
            </div>

          </div>

          <div class="form-group col-md-6">

            <div class="form-check">
              <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-overdue-music" value="OverdueRecallMusic">
              <label for="payment-type-overdue-music">Music Overdue/Recall Fine</label>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-lost-music" value="LostItemMusic">
              <label for="payment-type-lost-music">Music Lost Item</label>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" required="required" name="paytype" id="payment-type-damaged-music" value="DamagedItemMusic">
              <label for="payment-type-damaged-music">Music Damaged Item</label>
            </div>

          </div>

        </div>

      </div>

      <div class="form-group col-md-6">
        <label for="id_number">ID Number (NetID, Unique ID, Borrower Card):</label>
        <input class="form-control" type="text" name="id_number" id="id_number">
      </div>

      <div class="form-group col-md-6">
        <label for="amount">Amount:</label>
        <input class="form-control" type="text" name="amount" id="amount">
      </div>

      <div class="form-group col-md-6">
        <input class="btn btn-primary" type="submit" id="submit" value="Generate URL">
      </div>


      <div class="form-group pt-5">
        <label for="output">The URL:</label>
        <textarea class="form-control" id="output" name="output" readonly> </textarea>
      </div>

    </form>

  </div>

</div>



<script type="text/javascript">

$( "#submit" ).click(function() {

  var id_number = jQuery("#id_number").val();

  var amount = jQuery("#amount").val();

  var paytype = jQuery("input[name=paytype]:checked").val();

  $('#output').text('https://payments.library.duke.edu/east/index.php?');

  if (id_number != '') {
    $('#output').append('id_number=' + id_number);
  }

  if (amount != '') {

    if (id_number != '') {
      $('#output').append('&');
    }

    $('#output').append('amount=' + amount);
  }

  if (paytype != '') {

    if (amount != '' || id_number != '') {
      $('#output').append('&');
    }

    $('#output').append('payment-type=' + paytype);
  }

});

</script>

</body>

</html>
