<html lang="en" dir="ltr">
<head>

	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">

    <title>Perkins &amp; Bostock Payments | Duke University Libraries</title>

    <?php include '../includes/header_scripts.php' ?>

</head>
<body>

  <div id="skip-link">
    <a href="#content" class="element-invisible element-focusable">Skip to main content</a>
  </div>

	<!-- HEADER -->
	<div class="header" role="banner">
		<!-- This is the basic masthead filler that is replaced via javascript -->
		<div id="dul-masthead-filler" style="width: 100%; text-align: center;">
		  <div class="dul-masthead-wrapper" style="height: 105px; width: 100%; max-width: 1220px; background-color:#053482; margin: 0 auto; margin-bottom: -2px;">
			  <a href="//library.duke.edu/"><img src="//library.duke.edu/masthead/img/logo.png" alt="Duke University Libraries" id="dul-masthead-logo" style="float: left; margin: 20px;"></a>
		  </div>
		</div>
	</div>

	<!-- BREADCRUMBS -->
	<div id="breadcrumbs" class="container">
		<?php include '../includes/breadcrumb_circ.php' ?>
	</div>


	<!-- CONTENT -->
	<div class="pt-4 pb-4 container bg-white" role="main" id="content">

		<div class="col-lg-12">

			<?php include '../includes/message.php' ?>

			<h1>Perkins &amp; Bostock Payments</h1>

			<div class="container">
				<div class="row pt-3 pb-2">
					<div class="col-md-8">
						<p>Use the following form to submit credit card payment.</p>
						<p>Questions about payments? For Perkins &amp; Bostock payments contact <a href="https://directory.library.duke.edu/staff/sonya.hinsdale">Sonya Hinsdale</a>.</p>
						<p>You can also read more about our <a href="https://library.duke.edu/using/borrowing#fines">fees for fines and lost items</a>.</p>
					</div>
					<div class="col-md-4 sidebar" role="complementary">
						<p>Learn more about <a href="https://directory.library.duke.edu/dept/access-and-delivery-services">Access and Delivery Services</a></p>
					</div>
				</div>
			</div>

			<hr class="dotted pb-2">

			<div class="alert alert-warning text-center" role="alert">
				Students may not use this service to pay for library fines. Instead please contact the <a href="http://finance.duke.edu/bursar/Payments/index.php" class="alert-link">Bursar's Office</a>.
			</div>

			<div class="row">

				<div class="col-md-1"></div>

				<div class="col-md-10">

					<form action="confirmation.php" method="post" id="myform" role="form">

					<!-- inlcude access_key -->
					<?php include '../includes/access_key.php' ?>

					<input type="hidden" name="transaction_uuid" value="<?php echo uniqid() ?>">
					<input type="hidden" name="merchant_defined_data1" id="mdd1" value="Perkins Bostock">
					<input type="hidden" name="merchant_defined_data2" id="mdd2" value="">
					<input type="hidden" name="merchant_defined_data3" id="mdd3" value="">
					<input type="hidden" name="merchant_defined_data4" id="mdd4" value="">
					<input type="hidden" name="merchant_defined_data5" id="mdd5" value="">
					<input type="hidden" name="signed_field_names" value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_forename,bill_to_surname,bill_to_company_name,bill_to_address_line1,bill_to_address_line2,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,bill_to_phone,bill_to_email,merchant_defined_data1,merchant_defined_data2,merchant_defined_data3,merchant_defined_data4,merchant_defined_data5">
					<input type="hidden" name="unsigned_field_names" value="">
					<input type="hidden" name="signed_date_time" value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
					<input type="hidden" name="locale" value="en">
					<input type="hidden" name="currency" value="USD">
					<input type="hidden" name="transaction_type" value="sale">
					<input type="hidden" name="reference_number" value="circulation-<?php echo gmdate("Ymd-His"); ?>">

					<fieldset class="payment-info">

						<legend class="pt-4">Payment Information</legend>

						<div class="form-row">

							<div class="form-group col-md-6 required">
								
								<label for="payment-type">I am paying for:</label>

								<div class="form-check">
									<input class="form-check-input" type="radio" required="required" name="payment-type" id="payment-type-overdue" value="Overdue Recall Fine" <?php if (! empty($_GET["payment-type"]) && $_GET["payment-type"] == "OverdueRecall") { echo ' checked'; } ?>>
									<label for="payment-type-overdue">an Overdue/Recall Fine</label>
								</div>

								<div class="form-check">
									<input class="form-check-input" type="radio" required="required" name="payment-type" id="payment-type-lost" value="Lost Item" <?php if (! empty($_GET["payment-type"]) && $_GET["payment-type"] == "LostItem") { echo ' checked';} ?>>
									<label for="payment-type-lost">a Lost Item Replacement and/or Processing Fee</label>
								</div>

								<div class="form-check">
									<input class="form-check-input" type="radio" required="required" name="payment-type" id="payment-type-damaged" value="Damaged Item" <?php if (! empty($_GET["payment-type"]) && $_GET["payment-type"] == "DamagedItem") { echo ' checked';} ?> >
									<label for="payment-type-damaged">a Damaged Item and/or Processing Fee</label>
								</div>

							</div>

						</div>

						<div class="form-row">
							<div class="form-group col-md-3 required">
								<label for="amount">Payment Amount</label>
								<input type="number" class="form-control" name="amount" id="amount" required="required" step='0.01' aria-describedby="amount-help" placeholder='00.00' <?php if (! empty($_GET['amount']) && $_GET['amount'] != "") { echo 'value="' . $_GET['amount'] .'"'; } ?> >
								<small id="amount-help" class="form-text text-muted">In US Dollars ($)</small>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-5 required">
								<label for="id_number">NetID, Unique ID, or Borrower Card Number</label>
								<input type="text" class="form-control" name="id_number" id="id_number" required="required" <?php if (! empty($_GET['id_number']) && $_GET['id_number'] != "") { echo 'value="' . $_GET['id_number'] .'"'; } ?> >
							</div>
						</div>
							
					</fieldset>


					<?php include "../includes/form-user-information.php" ?>

					<div class="form-group submit-wrapper pt-3">
						<input type="submit" class="btn btn-primary" value="Continue to Confirm Information &raquo;">
					</div>

					</form>

				</div>

			</div>

		</div>

	</div>

	<!-- FOOTER -->
	<?php include "../includes/footer.php" ?>

</div>


<script type="text/javascript">

jQuery(document).ready(function(){


	// change payment value to 2 decimals

	jQuery('#amount').on('blur', function() {

		var amount = jQuery('#amount').val().replace(/^\s+|\s+$/g, '');

		if( (jQuery('#amount').val() != '') && (!amount.match(/^$/) )){
			jQuery('#amount').val( Number(amount).toFixed(2));
		}

	});


	// verify emails match

	jQuery("#bill_to_email_confirmation").change(function(){

		var email1 = jQuery("#bill_to_email").val();

		var email2 = jQuery("#bill_to_email_confirmation").val();

		if ( email2 != email1 ) {
			document.getElementById("bill_to_email_confirmation").setCustomValidity("Email addresses must match");
		}

		if ( email2 == email1 ) {
			document.getElementById("bill_to_email_confirmation").setCustomValidity("");
		}

	});

	// update state/province

	jQuery("#bill_to_address_country").change(function() {

		// USA
		if (jQuery(this).val() == "US") {

			jQuery("#usa").show();
			jQuery("#canada").hide();
			jQuery("#international").hide();

			jQuery("#bill_to_address_state").prop('disabled', false);
			jQuery("#bill_to_address_state").prop('required', 'required');

			jQuery("#bill_to_address_state_ca").prop('disabled', 'disabled');
			jQuery("#bill_to_address_state_ca").prop('required', false);


		}

		// Canada
		if (jQuery(this).val() == "CA") {

			jQuery("#usa").hide();
			jQuery("#canada").show();
			jQuery("#international").hide();

			jQuery("#bill_to_address_state").prop('disabled', 'disabled');
			jQuery("#bill_to_address_state").prop('required', false);

			jQuery("#bill_to_address_state_ca").prop('disabled', false);
			jQuery("#bill_to_address_state_ca").prop('required', 'required');

		}

		// Others
		if (jQuery(this).val() != "US") {

			if (jQuery(this).val() != "CA") {

				jQuery("#usa").hide();
				jQuery("#canada").hide();
				jQuery("#international").show();

				jQuery("#bill_to_address_state").prop('disabled', 'disabled');
				jQuery("#bill_to_address_state").prop('required', false);

				jQuery("#bill_to_address_state_ca").prop('disabled', 'disabled');
				jQuery("#bill_to_address_state_ca").prop('required', false);

			}

		}

	});

});


</script>

</body>
</html>
