<footer id="footer" class="page__footer pt-4" role="contentinfo">
  <div class="container">
    <div class="row">
      <div class="col-lg-1 mb-4">
        <a onclick="_paq.push(['trackEvent', 'Footer', 'Branding', 'Library Logo']);" href="https://library.duke.edu"><img alt="Duke University Libraries" src="https://library.duke.edu/themes/custom/drupal9_dulcet/images/footer/devillogo-60pct-60.png"></a>
      </div>
      <div class="col-lg-2 mb-4">
        <h2><a onclick="_paq.push(['trackEvent', 'Footer', 'Contact Info', 'Contact Us']);" href="https://library.duke.edu/about/contact">Contact Us</a></h2>
        <div>
          <small>411 Chapel Drive<br>Durham, NC 27708<br>(919) 660-5870<br>Perkins Library Service Desk</small>
        </div>
      </div>
      <div class="col-lg-4 mb-4">
        <h2><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'Services Portal']);" href="https://library.duke.edu/services">Services for...</a></h2>
          <div class="row">
            <div class="col-lg-6">
              <ul class="list-unstyled">
                <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'Faculty Instructors']);" href="https://library.duke.edu/services/faculty">Faculty &amp; Instructors</a></li>
                <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'Graduate Students']);" href="https://library.duke.edu/services/graduate">Graduate Students</a></li>
                <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'Undergraduate Students']);" href="https://library.duke.edu/services/undergraduate">Undergraduate Students</a></li>
                <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'International Students']);" href="https://library.duke.edu/services/international">International Students</a></li>
              </ul>
            </div>
            <div class="col-lg-6">
              <ul class="list-unstyled">
                <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'Alumni']);" href="https://library.duke.edu/services/alumni">Alumni</a></li>
                <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'Donors']);" href="https://library.duke.edu/services/donors">Donors</a></li>
                <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'Visitors']);" href="https://library.duke.edu/services/visitors">Visitors</a></li>
                <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Services', 'Patrons with Disabilities']);" href="https://library.duke.edu/services/disabilities">Patrons with Disabilities</a></li>
              </ul>
            </div>
          </div>
      </div>
      <div class="col-lg-3 mb-4">
        
        <div class="social-media-icons">

          <a onClick="_paq.push(['trackEvent', 'Footer', 'Social', 'Twitter']);" href="https://twitter.com/DukeLibraries" title="Twitter"><i class="fab fa-twitter"></i><span class="sr-only">Twitter</span></a>

          <a onClick="_paq.push(['trackEvent', 'Footer', 'Social', 'Facebook']);" href="https://www.facebook.com/dukelibraries" title="Facebook"><i class="fab fa-facebook"></i><span class="sr-only">Facebook</span></a>

          <a onClick="_paq.push(['trackEvent', 'Footer', 'Social', 'Youtube']);" href="https://www.youtube.com/user/DukeUnivLibraries" title="Youtube"><i class="fab fa-youtube"></i><span class="sr-only">Youtube</span></a>

          <a onClick="_paq.push(['trackEvent', 'Footer', 'Social', 'Flickr']);" href="https://www.flickr.com/photos/dukeunivlibraries/" title="Flickr"><i class="fab fa-flickr"></i><span class="sr-only">Flickr</span></a>

          <a onClick="_paq.push(['trackEvent', 'Footer', 'Social', 'Instagram']);" href="https://instagram.com/dukelibraries" title="Instagram"><i class="fab fa-instagram"></i><span class="sr-only">Instagram</span></a>

          <a onClick="_paq.push(['trackEvent', 'Footer', 'Social', 'RSS']);" href="https://blogs.library.duke.edu/" title="RSS"><i class="fas fa-rss-square"></i><span class="sr-only">RSS</span></a>

        </div>

        <ul class="list-unstyled">
          <li><a href="/about/newsletter" onclick="_paq.push(['trackEvent', 'Footer', 'Links', 'Email List']);">Sign Up for Our Newsletter</a></li>
          <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Links', 'Re-use and Attribution']);" href="/about/reuse-attribution">Re-use &amp; Attribution</a> / <a onclick="_paq.push(['trackEvent', 'Footer', 'Links', 'Privacy']);" href="/about/privacy">Privacy</a></li>
          <li><a href="/about/statement-potentially-harmful-language-library-descriptions" onclick="_paq.push(['trackEvent', 'Footer', 'Links', 'Harmful Language Statement']);">Harmful Language Statement</a></li>
          <li><a onclick="_paq.push(['trackEvent', 'Footer', 'Links', 'Support the Libraries']);" href="/support">Support the Libraries</a></li>
        </ul>

        <a onclick="_paq.push(['trackEvent', 'Footer', 'Links', 'Creative Commons']);" rel="license" href="https://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="https://licensebuttons.net/l/by-nc-sa/3.0/80x15.png" title="This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License"></a>
      </div>
      <div class="col-lg-2 mb-4">
        <div id="duke-logo">
          <a onclick="_paq.push(['trackEvent', 'Footer', 'Branding', 'Duke Logo']);" href="https://www.duke.edu"><img src="https://library.duke.edu/themes/custom/drupal9_dulcet/images/footer/dukelogo_vert_60pct_140.png" alt="Duke University"></a>
        </div>

              <div>
        </div>
      </div>
    </div>
  </div>
</footer>
