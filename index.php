<html lang="en" dir="ltr">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Payments | Duke University Libraries</title>

		<?php include 'includes/header_scripts.php' ?>

</head>
<body>

  <div id="skip-link">
    <a href="#content" class="element-invisible element-focusable">Skip to main content</a>
  </div>

	<!-- HEADER -->
	<div class="header" role="banner">
		<div class="container-fluid">
			<!-- This is the basic masthead filler that is replaced via javascript -->
			<div id="dul-masthead-filler" style="width: 100%; text-align: center;">
				<div class="dul-masthead-wrapper" style="height: 105px; width: 100%; max-width: 1220px; background-color:#053482; margin: 0 auto; margin-bottom: -2px;">
					<a href="//library.duke.edu/"><img src="//library.duke.edu/masthead/img/logo.png" alt="Duke University Libraries" id="dul-masthead-logo" style="float: left; margin: 20px;"></a>
				</div>
			</div>
		</div>
	</div>

	<!-- BREADCRUMBS -->
	<div id="breadcrumbs" class="container">
		<?php include 'includes/breadcrumb.php' ?>
	</div>

	<!-- CONTENT -->
	<div class="pt-4 pb-4 container bg-white" role="main" id="content">

		<div class="col-lg-12">

			<?php include 'includes/message.php' ?>

			<h1>Duke University Libraries Payments</h1>

			<div class="row pt-5">

				<div class="col-lg-1"></div>

				<div class="col-lg-10">

					<legend>I would like to</legend>

					<div class="row home-buttons">

						<div class="form-button col-md-6 pb-4">
							<a class="btn btn-block btn-primary btn-lg pt-3 pb-3" href="circulation"><i class="fas fa-dollar-sign"></i> Pay Perkins &amp; Bostock Fines/Fees</a>
						</div>

						<div class="form-button col-md-6 pb-4">
							<a class="btn btn-block btn-primary btn-lg pt-3 pb-3" href="rubenstein"><i class="fas fa-book"></i> Pay a Rubenstein Library invoice</a>
						</div>

						<div class="form-button col-md-6 pb-4">
							<a class="btn btn-block btn-primary btn-lg pt-3 pb-3" href="dds"><i class="fas fa-truck"></i> Pay an Interlibrary Request invoice</a>
						</div>

						<div class="form-button col-md-6 pb-4">
							<a class="btn btn-block btn-primary btn-lg pt-3 pb-3" href="dpc"><i class="fas fa-images"></i> Pay a Digital Production Center invoice</a>
						</div>

						<div class="form-button col-md-6 pb-4">
							<a class="btn btn-block btn-primary btn-lg pt-3 pb-3" href="east"><i class="fas fa-compact-disc"></i> Pay a Lilly or Music Fines/Fees</a>
						</div>

						<div class="form-button col-md-6 pb-4">
							<a class="btn btn-block btn-primary btn-lg pt-3 pb-3" href="other"><i class="fas fa-cog"></i> Pay for something else</a>
						</div>

					</div>


				</div>

			</div>

			<hr class="dotted">

			<div class="alert alert-warning text-center" role="alert">
				Students may not use this service to pay for library fines. Instead please contact the <a href="http://finance.duke.edu/bursar/Payments/index.php" class="alert-link">Bursar's Office</a>.
			</div>


			<p>Questions about payments? For Rubenstein Library payments, contact <a href="https://directory.library.duke.edu/staff/towanda.wilson">Towanda Wilson</a>. For Access and Delivery Services payments, contact <a href="https://directory.library.duke.edu/staff/sonya.hinsdale">Sonya Hinsdale</a>. For all other issues, please contact <a href="https://directory.library.duke.edu/staff/jameca.dupree">Jameca Dupree</a>.</p>

		</div>

	</div>


	<!-- FOOTER -->
	<?php include "includes/footer.php" ?>



</body>
</html>
